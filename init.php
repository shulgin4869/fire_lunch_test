<?php

if (!file_exists(__DIR__ . '/config/config.php')) {
    die('Config not found');
}

require_once(__DIR__ . '/config/config.php');

spl_autoload_register(function ($class_name) {
    $class = __DIR__ . '/class/' . $class_name . '.php';
    if (file_exists($class)) {
        require_once($class);
    } else {
        // todo: тут можно и не выбрасывать ошибку, тк стек автозагрузки может содержать несколько функций
        die("Class $class_name not found");
    }
});