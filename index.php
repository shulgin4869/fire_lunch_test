<?php

require_once __DIR__ . '/init.php';

$params = [
    'date_from' => '2020-03-01',
    'date_to' => '2020-03-10',
    'manufacture_id' => 5,
];

print_r(Report::getReport($params));