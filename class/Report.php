<?php

/**
 * Class Report
 */
Class Report
{
    /**
     * Get report
     *
     * @param array $params
     * @return array
     */
    public static function getReport(array $params)
    {
        $data = $where = [];
        $db_link = DB::getInstance()->link();

        if (!empty($params['date_from'])) {
            $where[] = sprintf(' AND STR_TO_DATE(nak_date, "%%d.%%m.%%Y") >= "%s"',
                mysql_real_escape_string($params['date_from'], $db_link));
        }

        if (!empty($params['date_to'])) {
            $where[] = sprintf(' AND STR_TO_DATE(nak_date, "%%d.%%m.%%Y") <= "%s"',
                mysql_real_escape_string($params['date_to'], $db_link));
        }

        if (!empty($params['manufacture_id'])) {
            $where[] = ' AND manufacture_id = ' . (int)$params['manufacture_id'];
        }

        $sql = sprintf('
            SELECT 
              client_id, nak_date, nak_data
            FROM
              fl_acc_nak_out
            WHERE 
              1 %s
            ORDER BY client_id ASC, STR_TO_DATE(nak_date, "%%d.%%m.%%Y") ASC
            ', implode(" ", $where)
        );

        $result = mysql_query($sql, $db_link) or die (DB::getInstance()->error());

        while ($row = mysql_fetch_assoc($result)) {
            $row['nak_data'] = json_decode($row['nak_data'], true);

            if (!is_array($row['nak_data'])) {
                // todo: warning
                continue;
            }

            foreach ($row['nak_data'] as $nak_data_row) {
                $data[$row['client_id']][$row['nak_date']][] = [
                    'lunchName' => isset($nak_data_row['lunchName']) ? $nak_data_row['lunchName'] : 'Неизвестно',
                    'amount' => isset($nak_data_row['amount']) ? $nak_data_row['amount'] : 0,
                ];
            }
        }

        return $data;
    }
}
