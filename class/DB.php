<?php

/**
 * Class DB
 */
Class DB extends Singleton
{
    /**
     * DB link
     * @var resource
     */
    protected $link = null;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
    }

    /**
     * Get link
     *
     * @return resource
     */
    public function link()
    {
        return $this->link;
    }

    /**
     * Create connect
     *
     * @param $server
     * @param $username
     * @param $password
     * @param bool $new_link
     * @param int $client_flags
     * @return resource
     */
    public function connect($server, $username, $password, $new_link = FALSE, $client_flags = 0)
    {
        $this->link = mysql_connect($server, $username, $password, $new_link, $client_flags) or die (self::error());
        mysql_set_charset('utf8', $this->link) or die (self::error());
        mysql_select_db(DB_NAME, $this->link) or die (self::error());

        return $this->link;
    }

    /**
     * Error report
     */
    public function error()
    {
        // todo: catch the error
        die(mysql_error($this->link));
    }
}