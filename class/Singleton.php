<?php

/**
 * Class Singleton
 */
abstract class Singleton {

    /**
     * Singleton instances
     *
     * @var array
     */
    private static $instances = array();

    /**
     * Create singleton object
     *
     * @return object
     */
    public static final function getInstance() {

        static $instance = null;

        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * @see self::getInstance()
     */
    public static final function get_instance() {
        return static::getInstance();
    }

    /**
     * Forbidden constructor
     *
     * @see Magic method __construct()
     */
    protected function __construct() { }

    /**
     * Forbidden unserialize
     *
     * @see Magic method __wakeup()
     */
    protected function __wakeup() { }

    /**
     * Forbidden serialize
     *
     * @see Magic method __sleep()
     */
    protected function __sleep() { }

    /**
     * Forbidden var_export
     *
     * @see Magic method __set_state()
     */
    protected function __set_state() { }

    /**
     * Forbidden clone object
     *
     * @see Magic method __clone()
     */
    protected function __clone() { }
}